package edu.rit.se.nvip_api;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import edu.rit.se.nvip.cvss.ExploitabilityPredictor;
import edu.rit.se.nvip.cvss.SeverityPredictor;

public class ModelRepository {

	static SeverityPredictor severityPredictor = null;
	static ExploitabilityPredictor exploitabilityPredictor = null;

	/**
	 * get severity predictor, run once!
	 * 
	 * @param repoDataPath TODO
	 * 
	 * @return
	 */
	public static SeverityPredictor getSeverityPredictorInstance(String repoDataPath) {
		if (severityPredictor == null) {
			synchronized (ModelRepository.class) {
				if (severityPredictor == null)
					severityPredictor = new SeverityPredictor(repoDataPath);
			}
		}
		return severityPredictor;
	}

	/**
	 * get exploitability predictor instance, run once!
	 * 
	 * @param repoDataPath TODO
	 * 
	 * @return
	 */
	public static ExploitabilityPredictor getExploitabilityPredictorInstance(String repoDataPath) {
		if (exploitabilityPredictor == null) {
			synchronized (ModelRepository.class) {
				if (exploitabilityPredictor == null)
					exploitabilityPredictor = new ExploitabilityPredictor(repoDataPath);
			}
		}
		return exploitabilityPredictor;
	}

	private ModelRepository() {
	}
}
