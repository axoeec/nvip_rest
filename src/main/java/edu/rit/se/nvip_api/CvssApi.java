package edu.rit.se.nvip_api;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

/**
 * Root resource (exposed at "exploitability" path)
 */

@Path("cvss")
public class CvssApi {

	@Context
	private ServletContext context;

	/**
	 * Method handling HTTP GET requests. The returned object will be sent to the
	 * client as "text/plain" media type.
	 *
	 * @return String that will be returned as a text/plain response.
	 */
	@GET
	@Produces(MediaType.TEXT_PLAIN)
	public String getInfo() {
		return "This is the NVIP REST API to query exploitability score for a given CVE description!";
	}

	@Path("severity")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getSeverity(@QueryParam("cve") String sCveDesc) {
		// String path = ApiConfig.properties.getProperty("dataRepo");
		String path = getRepoDataPath();
		String[] pred = ModelRepository.getSeverityPredictorInstance(path).predictSeverity(sCveDesc);
		String json = "{\"severity\":\"" + pred[0] + "\", \"probability\": \"" + pred[1] + "\", \"cve\":\"" + sCveDesc
				+ "\"}";
		return json;
	}

	@Path("exploitability")
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public String getExploitability(@QueryParam("cve") String sCveDesc) {
		// String path = ApiConfig.properties.getProperty("dataRepo");
		String path = getRepoDataPath();
		String[] pred = ModelRepository.getExploitabilityPredictorInstance(path).predictExploitability(sCveDesc);
		String json = "{\"exploit\":\"" + pred[0] + "\", \"probability\": \"" + pred[1] + "\", \"cve\":\"" + sCveDesc
				+ "\"}";
		return json;
	}

	/**
	 * get repo data path
	 * 
	 * @return
	 */
	private String getRepoDataPath() {
		String realPath = context.getRealPath("/WEB-INF/app.properties");
		Properties props = new Properties();
		try {
			props.load(new FileInputStream(new File(realPath)));
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		String value = props.getProperty("dataRepo");
		if (value == null) {
			value = "E:\\Projects\\NVIP\\nvipdatav2"; //default
		}
		return value;
	}

}
