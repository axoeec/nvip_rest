
National Vulnerability Intelligence Platform (NVIP) crawls a given list of vulnerability sources and creates a dynamic database of Common Vulnerabilities and Exposures (CVE). 

#### The REST API Project
- This Java/Maven project was developed to expose the internal functionalities of NVIP system to the public.
- The project includes the NVIP project as dependency. When you clone this Rest API project, please make sure you have the most recent version of the NVIP project as well. You need to run "mvn install" on the NVIP project, and make sure that it is included properly.
- Please run "mvn package" to create a .war file that can be deployed. Once you have the war file ready, copy it under the "webapps" directory of the Tomcat in the production/test environment and restart the Tomcat.
- Please make sure that the "dataRepo" attribute in the "app.properties" file under WEB-INF points to the data directory used by the NVIP project. This project is de-serializing previously trained AI/ML models under that directory!